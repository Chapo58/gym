<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ejercicio extends Model{

    use SoftDeletes;

    protected $table = 'ejercicios';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'descripcion', 'archivo_url'];

    protected $dates = ['deleted_at'];

    public static function getEjercicios(){
        $ejercicios = Ejercicio::whereNull('deleted_at')
            ->get();
        return $ejercicios;
    }

    public static function getEjerciciosArray(){
        $ejercicios = self::getEjercicios()->pluck('nombre', 'id')->all();
        return $ejercicios;
    }

    public function rutinas(){
        return $this->belongsToMany(Rutina::class)
            ->withPivot('series')
            ->withPivot('repeticiones');
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
