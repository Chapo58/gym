<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rutina extends Model{

    use SoftDeletes;

    protected $table = 'rutinas';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'descripcion'];

    protected $dates = ['deleted_at'];

    public static function getRutinas(){
        $rutinas = Rutina::whereNull('deleted_at')
            ->get();
        return $rutinas;
    }

    public static function getRutinasArray(){
        $rutinas = self::getRutinas()->pluck('nombre', 'id')->all();
        return $rutinas;
    }

    public function ejercicios(){
        return $this->belongsToMany(Ejercicio::class)
            ->withPivot('series')
            ->withPivot('repeticiones');
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
