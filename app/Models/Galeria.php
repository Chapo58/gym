<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Galeria extends Model{

    use SoftDeletes;

    protected $table = 'galeria';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'archivo_url', 'slider'];

    protected $dates = ['deleted_at'];

    public static function getArchivos(){
        $archivos = Galeria::whereNull('deleted_at')
            ->orderBy('id','DESC')
            ->get();
        return $archivos;
    }

    public static function getArchivosArray(){
        $archivos = self::getArchivos()->pluck('nombre', 'id')->all();
        return $archivos;
    }

    public function __toString(){
        return (string) $this->nombre;
    }
}
