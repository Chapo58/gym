<?php

namespace App\Http\Controllers\GymAdmin;

use App\Classes\Reply;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Ejercicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Session;

class EjercicioController extends GymAdminBaseController{

    public function index(){
        $this->data['ejercicios'] = Ejercicio::getEjercicios();

        $this->data['title'] = "Ejercicios";

        return View::make('gym-admin.ejercicios.index', $this->data);
    }

    public function create(){
        $this->data['title'] = "Agregar Ejercicio";

        return view('gym-admin.ejercicios.create', $this->data);
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $nuevoEjercicio = new Ejercicio($requestData);

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenEjercicio($request);
        if($imagen_url != false){
            $nuevoEjercicio->archivo_url = $imagen_url;
        }

        $nuevoEjercicio->save();

        return Reply::redirect(route('gym-admin.ejercicios.index'), "Ejercicio guardado.");
    }

    public function show($id){
        $this->data['ejercicio'] = Ejercicio::findOrFail($id);
        $this->data['title'] = "Ver Ejercicio";

        return view('gym-admin.ejercicios.show', $this->data);
    }

    public function edit($id){
        $this->data['ejercicio'] = Ejercicio::findOrFail($id);
        $this->data['title'] = "Editar Ejercicio";

        return view('gym-admin.ejercicios.edit', $this->data);
    }

    public function update($id, Request $request){
        $ejercicio = Ejercicio::findOrFail($id);
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenEjercicio($request);
        if($imagen_url != false){
            $requestData['archivo_url'] = $imagen_url;
        }

        $ejercicio->update($requestData);

      //  return Reply::redirect(route('gym-admin.ejercicios.index'), "Ejercicio actualizado.");
        return redirect('gym-admin/ejercicios');
    }

    public function destroy($id){
        Ejercicio::destroy($id);

        return Reply::redirect(route('gym-admin.ejercicios.index'), "Ejercicio eliminado.");
    }
}
