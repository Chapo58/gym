<?php

namespace App\Http\Controllers\GymAdmin;

use App\Classes\Reply;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Models\Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Session;

class GaleriaController extends GymAdminBaseController{

    public function index(){
        $this->data['archivos'] = Galeria::getArchivos();

        $this->data['title'] = "Galeria";

        return View::make('gym-admin.galeria.index', $this->data);
    }

    public function create(){
        $this->data['title'] = "Agregar Imagen";

        return view('gym-admin.galeria.create', $this->data);
    }

    public function store(Request $request){
        $requestData = $request->all();

        $nuevoArchivo = new Galeria($requestData);

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenGaleria($request);
        $nuevoArchivo->archivo_url = $imagen_url;

        $nuevoArchivo->save();

        return Reply::redirect(route('gym-admin.galeria.index'), "Archivo Guardado.");
    }

    public function show($id){
        $this->data['archivo'] = Galeria::findOrFail($id);
        $this->data['title'] = "Ver Archivo";

        return view('gym-admin.galeria.show', $this->data);
    }

    public function edit($id){
        $this->data['archivo'] = Galeria::findOrFail($id);
        $this->data['title'] = "Editar Archivo";

        return view('gym-admin.galeria.edit', $this->data);
    }

    public function update($id, Request $request){
        $archivo = Galeria::findOrFail($id);
        $requestData = $request->all();

        $controllerImagen = new ImageController();
        $imagen_url = $controllerImagen->guardarImagenGaleria($request);
        if($imagen_url != false){
            $requestData['archivo_url'] = $imagen_url;
        }

        $archivo->update($requestData);

      //  return Reply::redirect(route('gym-admin.ejercicios.index'), "Archivo Actualizado.");
        return redirect('gym-admin/galeria');
    }

    public function destroy($id){
        Galeria::destroy($id);

        return Reply::redirect(route('gym-admin.galeria.index'), "Archivo Eliminado.");
    }
}
