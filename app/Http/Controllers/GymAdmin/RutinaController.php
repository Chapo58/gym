<?php

namespace App\Http\Controllers\GymAdmin;

use App\Classes\Reply;
use App\Models\Ejercicio;
use App\Models\Rutina;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Session;

class RutinaController extends GymAdminBaseController{

    public function index(){
        $this->data['rutinas'] = Rutina::getRutinas();

        $this->data['title'] = "Rutinas";

        return View::make('gym-admin.rutinas.index', $this->data);
    }

    public function create(){
        $this->data['title'] = "Agregar Rutina";
        $this->data['ejercicios'] = Ejercicio::getEjercicios();

        return view('gym-admin.rutinas.create', $this->data);
    }

    public function store(Request $request){
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        $nuevaRutina = new Rutina($requestData);
        $nuevaRutina->save();

        if(isset($requestData['ejercicios'])) {
            $ejercicios = $requestData['ejercicios'];
            foreach($ejercicios as $ejercicio){
                if(isset($ejercicio['ejercicio_id']) && !empty($ejercicio['ejercicio_id'])){
                    $nuevaRutina->ejercicios()->attach($ejercicio['ejercicio_id'], ['series' => $ejercicio['series'], 'repeticiones' => $ejercicio['repeticiones']]);
                }
            }
        }

        return Reply::redirect(route('gym-admin.rutinas.index'), "Rutina Guardada.");
    }

    public function show($id){
        $this->data['rutina'] = Rutina::findOrFail($id);
        $this->data['title'] = "Ver Rutina";

        return view('gym-admin.rutinas.show', $this->data);
    }

    public function edit($id){
        $this->data['rutina'] = Rutina::findOrFail($id);
        $this->data['title'] = "Editar Rutina";
        $this->data['ejercicios'] = Ejercicio::getEjercicios();

        return view('gym-admin.rutinas.edit', $this->data);
    }

    public function update($id, Request $request){
        $rutina = Rutina::findOrFail($id);
        $this->validate($request, [
    			'nombre' => 'required'
    		]);
        $requestData = $request->all();

        if(isset($requestData['ejercicios'])) {
            $rutina->ejercicios()->detach();
            $ejercicios = $requestData['ejercicios'];
            foreach($ejercicios as $ejercicio){
                if(isset($ejercicio['ejercicio_id']) && !empty($ejercicio['ejercicio_id'])){
                    $rutina->ejercicios()->attach($ejercicio['ejercicio_id'], ['series' => $ejercicio['series'], 'repeticiones' => $ejercicio['repeticiones']]);
                }
            }
        }

        $rutina->update($requestData);

      //  return Reply::redirect(route('gym-admin.rutinas.index'), "Rutina Actualizada.");
        return redirect('gym-admin/rutinas');
    }

    public function destroy($id){
        Rutina::destroy($id);

        return Reply::redirect(route('gym-admin.rutinas.index'), "Rutina Eliminada.");
    }
}
