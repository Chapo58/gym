<?php

namespace App\Http\Controllers\Merchant;

use App\Classes\Reply;
use App\Http\Controllers\MerchantBaseController;
use App\Http\Requests\Login\LoginRequest;
use App\Mail\FitsigmaEmailVerification;
use App\Models\GymSetting;
use App\Models\Merchant;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class MerchantsController extends MerchantBaseController
{

    public function index() {
        if ($this->data['userCheck']) {
            if (Auth::guard('merchant')->user()->is_admin != 1) {
                return Redirect::route('gym-admin.dashboard.index');
            } else {
                return Redirect::route('gym-admin.superadmin.dashboard');
            }
        }
        $this->data['gymSettings'] = GymSetting::first();
        return View::make("fitsigma.login", $this->data);
    }

    /**
     * Store a newly created merchant in storage.
     *
     * @return Response
     */
    public function store() {
        if (Request::ajax()) {
            $auth = false;

            $credentials = array(
                "username" => trim(Input::get('username')),
                "password" => trim(Input::get('password'))
            );


            if (Auth::guard('merchant')->attempt($credentials, true)) {
                $auth = true; // Success
                $message = 'Ingresando al sistema';
                $url = route('gym-admin.dashboard.index');
            } else {
                $message = 'Usuario o contraseña invalido';
                $url = '';
            }

            return Response::json(
                [
                    'success' => $auth,
                    'url' => $url,
                    'message' => $message
                ]
            );

        }

        return 'Illegal Request';
    }

    public function sendResetPasswordLink() {

        if(trim(Input::get('email')) == '') {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'El Email no puede estar vacio.'
                ]
            );
        }

        $merchant = Merchant::getByEmail(Input::get('email'));

        if(is_null($merchant)) {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Email no registrado.'
                ]
            );
        }
        else{
            $resetToken = str_random(40);
            $merchant->reset_password_token = $resetToken;
            $merchant->save();

            $email = Input::get('email');

            $eText = 'Este correo electrónico fue enviado automáticamente por Improve en respuesta a su solicitud para recuperar su contraseña. Esto se hace para tu protección. Solo usted, el destinatario de este correo electrónico puede dar el siguiente paso en el proceso de recuperación de contraseña.';

            $this->data['title'] = "Contraseña Olvidada";
            $this->data['mailHeading'] = "Restablecer Contraseña";
            $this->data['emailText'] = $eText;

            $this->data['url'] = url("/merchant/reset/" . $resetToken);

            Mail::to($email)->send(new FitsigmaEmailVerification($this->data));

            return Response::json(
                [
                    'success' => true,
                    'message' => 'Revisa tu bandeja de entrada para ver el enlace de restablecimiento de contraseña.'
                ]
            );

        }

    }

    public function resetPassword($token) {

        $this->data['merchant'] = Merchant::where('reset_password_token', $token)->first();

        if(is_null($this->data['merchant'])) {
            abort(403);
        }
        $this->data['gymSettings'] = GymSetting::first();

        return view('fitsigma.reset', $this->data);
    }

    public function updatePassword() {
        $inputData = Input::get('formData');
        parse_str($inputData, $formFields);

        if(trim($formFields['password']) == '') {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'La contraseña no puede estar vacia.'
                ]
            );
        }
        elseif($formFields['password'] != $formFields['confirm_password']) {
            return Response::json(
                [
                    'success' => false,
                    'message' => 'Las contraseñas no coinciden.'
                ]
            );
        }
        elseif($formFields['password'] == $formFields['confirm_password']) {

            $merchant = Merchant::where('reset_password_token', $formFields['reset_token'])->first();

            if(is_null($merchant)) {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Token de restablecimiento invalido.'
                    ]
                );
            }

            $merchant->reset_password_token = '';
            $merchant->password = Hash::make($formFields['password']);
            $merchant->save();

            return Response::json(
                [
                    'success' => true,
                    'message' => 'La contraseña se ha restablecido con éxito.<br> Click <strong><a href="'.route('merchant.login.index').'">aqui</a></strong> para logearte.'
                ]
            );
        }


        $merchant = Merchant::getByEmail($formFields['email']);
    }
}
