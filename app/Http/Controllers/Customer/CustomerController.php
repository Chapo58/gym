<?php

namespace App\Http\Controllers\Customer;

use App\Classes\Reply;
use App\Http\Requests\Customer\LoginStoreRequest;
use App\Http\Requests\Customer\RegisterStoreRequest;
use App\Http\Requests\Customer\ResetPasswordRequest;
use App\Http\Requests\Customer\UpdatePasswordRequest;
use App\Mail\FitsigmaEmailVerification;
use App\Models\BusinessCustomer;
use App\Models\Common;
use App\Models\GymClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;

class CustomerController extends CustomerBaseController{

    public function index(){
        if ($this->data['customerCheck']) {
            return Redirect::route('customer-app.dashboard.index');
        }
        return view('customer.login', $this->data);
    }

    public function store(LoginStoreRequest $request){
        $email = $request->email;
        $password = $request->password;

        if(Auth::guard('customer')->attempt(['email' => $email, 'password' => $password], true)) {
            return Reply::redirect(route('customer-app.dashboard.index'), 'Ingresando a su panel');
        }

        return Reply::error('Usuario o Contraseña Incorrectos');
    }

    public function register(){
        $this->data['branches'] = Common::all();

        return view('customer.register', $this->data);
    }

    public function registerStore(RegisterStoreRequest $request){
        $customer = new GymClient();
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->email = $request->email;
        $customer->password = Hash::make($request->password);
        $customer->save();

        $business = new BusinessCustomer();
        $business->detail_id = $request->branch_id;
        $business->customer_id = $customer->id;
        $business->save();

        return Reply::redirect(route('customer.index'), 'El cliente ha sido registrado correctamente.');
    }

    public function sendResetPasswordLink(ResetPasswordRequest $request){
        $customer = GymClient::where('email','=', $request->email)->first();

        if(is_null($customer)) {
            return Reply::error('Email no registrado');
        } else {
            $resetToken = str_random(40);
            $customer->reset_password_token = $resetToken;
            $customer->save();

            $email = $request->email;

            $eText = 'Este correo electrónico fue enviado automáticamente por Improve en respuesta a su solicitud para recuperar su contraseña. Esto es por tu protección. Solo usted, el destinatario de este correo electrónico puede dar el siguiente paso en el proceso de recuperación de contraseña.';

            $this->data['title'] = "Contraseña Olvidada";
            $this->data['mailHeading'] = "Restablecer Contraseña";
            $this->data['emailText'] = $eText;

            $this->data['url'] = url("/customer/reset/" . $resetToken);

            Mail::to($email)->send(new FitsigmaEmailVerification($this->data));

            return Reply::success('Revisa tu email para utilizar el link de recuperación.');
        }
    }


    public function resetPassword($token){
        $this->data['customer'] = GymClient::where('reset_password_token', '=', $token)->first();

        if(is_null($this->data['customer'])) {
            abort(403);
        }

        return view('customer.reset', $this->data);
    }

    public function updatePassword(UpdatePasswordRequest $request){
        $customer = GymClient::where('reset_password_token', '=', $request->reset_token)->first();
        $customer->reset_password_token = '';
        $customer->password = Hash::make($request->password);
        $customer->save();

        return Reply::redirect(route('customer.index'), 'La contraseña se ha restablecido con existo');
    }

    public function redirectToProvider($provider, $id = null){
        return Socialite::driver($provider)
//            ->with(['branch_id' => $id])
            ->redirect();
    }

    public function handleProviderCallback($provider){
        $user = Socialite::driver($provider)->user();
        $customer = GymClient::where('email', '=', $user->email)->first();
//        print_r($provider);
//        print_r($customer);
//        die;

        if (is_null($customer))
        {
            $customer = new GymClient();
            $customer->first_name = $user->name;
            $customer->email = $user->email;
            $customer->save();

//            $business = new BusinessCustomer();
//            $business->detail_id = $request->branch_id;
//            $business->customer_id = $customer->id;
//            $business->save();

            Auth::guard('customer')->login($user);

            return Redirect::route('customer-app.dashboard.index');
        } elseif($user->email == $customer->email) {
            $customer->first_name = $user->name;
            $customer->email = $user->email;
            $customer->save();
            Auth::guard('customer')->login($user);

            return Redirect::route('customer-app.dashboard.index');
        }
    }

    public function customerLogout(){
        $user  = Auth::guard('customer')->user();
        unset($user->detail_id);
        Auth::guard('customer')->logout();

        return Redirect::route('customer.index');
    }

}
