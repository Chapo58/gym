<?php

namespace App\Http\Controllers\Customer;

use App\Classes\Reply;
use App\Models\Ejercicio;
use App\Models\GymClient;
use App\Models\Rutina;

class CustomerRutinaController extends CustomerBaseController{

    public function index(){
        $this->data['title'] = 'Rutina de Ejercicios';
        $this->data['rutinasMenu'] = 'active';

        if($this->data['customerValues']->rutina){
            return view('customer-app.rutina.index', $this->data);
        } else {
            $this->data['rutinas'] = Rutina::getRutinas();
            return view('customer-app.rutina.rutinas', $this->data);
        }
    }

    public function listadoRutinas(){
        $this->data['title'] = 'Listado de Rutinas';
        $this->data['rutinasMenu'] = 'active';
        $this->data['rutinas'] = Rutina::getRutinas();

        return view('customer-app.rutina.rutinas', $this->data);
    }


    public function show($id){
        $this->data['ejercicio'] = Ejercicio::find($id);

        return view('customer-app.rutina.show', $this->data);
    }

    public function asignarRutina($id){
        $cliente = GymClient::find($this->data['customerValues']->id);
        $rutina = Rutina::find($id);

        if($rutina){
            $cliente->rutina_id = $rutina->id;
            $cliente->save();
            return redirect('customer-app/rutinas');
        } else {
            return redirect('customer-app/rutinas/rutinas');
        }
    }

}
