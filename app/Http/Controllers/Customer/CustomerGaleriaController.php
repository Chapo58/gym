<?php

namespace App\Http\Controllers\Customer;

use App\Models\Galeria;

class CustomerGaleriaController extends CustomerBaseController{

    public function index(){
        $this->data['title'] = 'Galeria de Imagenes';
        $this->data['galeriaMenu'] = 'active';
        $this->data['imagenes'] = Galeria::getArchivos();

        return view('customer-app.galeria.index', $this->data);
    }

}
