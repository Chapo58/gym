<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddSubscriptionNotification extends Notification
{
    use Queueable;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $via = ['database', 'mail'];
        return $via;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/customer');

        return (new MailMessage)
            ->subject( 'Improve Cliente App - Notificación de Suscripción')
            ->greeting('Hola '.ucwords($notifiable->name).'!')
            ->line('Tu suscripción ha sido aceptada por un administrador.')
            ->action('Ingresar al Sistema', $url)
            ->line('Gracias por utilizar nuestro sistema!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'customer_id' => $notifiable->id,
            'notification_type' => 'Suscripción Agregada',
            'title' => 'Suscripción agregada correctamente.'
        ];
    }
}
