@extends('layouts.merchant.login')

@section('content')

    <!-- BEGIN FORGOT PASSWORD FORM -->
    {!! Form::open(array('route' => ['merchant.login.update-password'], 'method' => 'POST', "id" => "update-password-form", "class" => 'login-form')) !!}
    <h3>Asistente para restablecer contraseña</h3>

    <div class="alert display-hide">

    </div>

    <div id="reset-form-fields">
        <p> Ingresa tu nueva contraseña. </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-key"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Nueva Contraseña" name="password" /> </div>
        </div>

        <p> Repite tu nueva contraseña. </p>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-key"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirmar Contraseña" name="confirm_password" /> </div>
        </div>

        <input type="hidden" name="reset_token" value="{{ $merchant->reset_password_token }}">

        <div class="form-actions">
            <button type="submit" class="btn green pull-right"> Aceptar </button>
        </div>

    </div>
    {!! Form::close() !!}
    <!-- END FORGOT PASSWORD FORM -->

@stop