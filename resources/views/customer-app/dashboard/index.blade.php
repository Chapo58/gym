@extends('layouts.customer-app.basic')

@section('title')
    Improve.fit | Panel de Clientes
@endsection

@section('CSS')
    {!! HTML::style('fitsigma_customer/bower_components/morrisjs/morris.css') !!}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Inicio</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menu Principal</li>
                <li class="active">Inicio</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        @if($slider)
        <div class="col-lg-6 col-xs-12">
            <div class="white-box">

                <div id="carouselExampleIndicators3" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php $contador = 0; ?>
                        @foreach($slider as $imagen)
                            <li data-target="#carouselExampleIndicators3" data-slide-to="{{$contador}}" {{($contador == 0) ? 'class="active"' : ''}}></li>
                            <?php $contador++; ?>
                        @endforeach
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        @foreach($slider as $imagen)
                            <div class="carousel-item {{($loop->first) ? 'active' : ''}}">
                                <img class="img-responsive" src="{{$imagen->archivo_url}}" style="width:100%;" alt="{{$imagen->nombre}}">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3 class="text-white">{{$imagen->nombre}}</h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators3" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators3" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        @endif
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <div class="card text-center">
                <div class="card-header">
                    Rutina Actual
                </div>
                <div class="card-body">
                    @if($customerValues->rutina)
                        <h4 class="card-title">{{$customerValues->rutina}}</h4>
                        <p class="card-text">{{$customerValues->rutina->descripcion}}</p>
                        <a href="{{ route('customer-app.rutinas.index') }}" class="btn btn-info m-t-10">Ver Rutina</a>
                        <hr>
                    @else
                        <h4 class="card-title">Todavia no tenes ninguna rutina asignada</h4>
                        <a href="{{url('customer-app/rutinas/rutinas')}}" class="btn btn-info m-t-10">Ver Rutinas</a>
                        <hr>
                    @endif
                </div>
            </div>
        </div>

        @if($proximoVencimiento)
        <div class="col-lg-4 col-md-4 col-xs-12 m-t-15">
            <div class="card card-inverse card-warning">
                <div class="box bg-warning text-center text-white">
                    <h2 class="text-white">Proximo Vencimiento</h2>
                    <h3 class="font-weight-bold text-white">{{ $proximoVencimiento->expires_on->format('d/m/Y') }} - <i class="fa {{ $gymSettings->currency->symbol }}"></i> {{ $proximoVencimiento->purchase_amount }}</h3>
                </div>
            </div>
        </div>
        @endif

    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="box-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Pagos Atrasados</span>
                        <span class="caption-helper"></span>
                    </div>
                </div>
                <div class="box-body flip-scroll">

                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr class="uppercase">
                            <th> # </th>
                            <th> Nombre </th>
                            <th> Importe </th>
                            <th> Fecha </th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($duePayments as $key=>$payment)
                            <tr>
                                <td> {{ $key+1 }} </td>
                                <td>
                                    {{ ucwords($payment->first_name.' '.$payment->last_name) }} </td>
                                <td> <i class="fa {{ $gymSettings->currency->symbol }}"></i>{{ $payment->amount_to_be_paid - $payment->paid }} </td>
                                <td>
                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', $payment->due_date)->toFormattedDateString() }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">No hay pagos atrasados.</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="box-title">
                    <div class="caption ">
                        <span class="caption-subject font-dark bold uppercase">Suscripciones a vencer en los proximos 45 dias</span>
                        <span class="caption-helper"></span>
                    </div>
                </div>
                <div class="box-body flip-scroll">

                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content">
                        <tr class="uppercase">
                            <th> # </th>
                            <th> Cliente </th>
                            <th> Vence el </th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($expiringSubscriptions as $key=>$expSubs)
                            <tr>
                                <td> {{ $key+1 }} </td>
                                <td>
                                    {{ ucwords($expSubs->first_name.' '.$expSubs->last_name) }} </td>
                                <td>
                                    {{ $expSubs->expires_on->format('d M, Y') }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td></td>
                                <td>No hay suscripciones a vencer.</td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">Grafico de Pagos</h3>
                <div id="morris-bar-chart"></div>
            </div>
        </div>
    </div>
@endsection

@section('JS')
    {!! HTML::script('fitsigma_customer/bower_components/raphael/raphael-min.js') !!}
    {!! HTML::script('fitsigma_customer/bower_components/morrisjs/morris.js') !!}

    <script>
        var months = [];
        months['1'] = 'Ene';
        months['2'] = 'Feb';
        months['3'] = 'Mar';
        months['4'] = 'Abr';
        months['5'] = 'May';
        months['6'] = 'Jun';
        months['7'] = 'Jul';
        months['8'] = 'Ago';
        months['9'] = 'Sep';
        months['10'] = 'Oct';
        months['11'] = 'Nov';
        months['12'] = 'Dic';
        Morris.Bar({
            element: 'morris-bar-chart',
            data: [
                @foreach($paymentCharts as $chart)
                {
                    "Mes": months['{{$chart->M}}'],
                    "Importe": '{{$chart->S}}'
                },
                @endforeach
            ],
            xkey: 'Month',
            ykeys: ['Income'],
            labels: ['Importe'],
            barColors:['#b8edf0', '#b4c1d7'],
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true
        });
    </script>
@endsection