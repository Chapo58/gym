@extends('layouts.customer-app.basic')

@section('title')
    Improve | Galeria
@endsection

@section('CSS')
    {!! HTML::style('admin/global/plugins/sweetalert2/sweetalert2.min.css') !!}
    {!! HTML::style('fitsigma_customer/bower_components/popup/magnific-popup.css') !!}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><i class="fa fa-image"></i> Galeria de Imagenes</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menú Principal</li>
                <li class="active">Galeria</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="card-columns el-element-overlay">
        @foreach($imagenes as $imagen)
            <div class="card">
                <div class="el-card-item">
                    <div class="el-card-avatar el-overlay-1">
                        <a class="image-popup-vertical-fit" href="{{$imagen->archivo_url}}"> <img src="{{$imagen->archivo_url}}" alt="{{$imagen->nombre}}" /> </a>
                    </div>
                    <div class="el-card-content">
                        <h3 class="box-title">{{$imagen->nombre}}</h3>
                        <br/> </div>
                </div>
            </div>
        @endforeach
    </div>

@endsection

@section('JS')
    {!! HTML::script('admin/global/plugins/sweetalert2/sweetalert2.min.js') !!}
    {!! HTML::script('fitsigma_customer/bower_components/popup/jquery.magnific-popup.min.js') !!}
    {!! HTML::script('fitsigma_customer/bower_components/popup/jquery.magnific-popup-init.js') !!}
@endsection