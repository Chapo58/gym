@extends('layouts.customer-app.basic')

@section('title')
    Improve | Rutina
@endsection

@section('CSS')
    {!! HTML::style('admin/global/plugins/sweetalert2/sweetalert2.min.css') !!}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Rutinas</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menú Principal</li>
                <li class="active">Rutinas</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-12 m-t-30">
            <h4 class="m-b-0">Listado de Rutinas</h4></div>
            @foreach($rutinas as $item)
                <div class="col-md-4 m-t-15">
                    <div class="card text-center">
                        <div class="card-body">
                            <h4 class="card-title">{{$item->nombre}}</h4>
                            <p class="card-text">{{$item->descripcion}}</p>
                            <hr>
                            <a href="{{url('customer-app/rutinas/asignar/'.$item->id)}}" class="btn btn-info">Seleccionar Rutina</a>
                            <hr>
                        </div>
                    </div>
                </div>
            @endforeach
    </div>
@endsection

@section('JS')
    {!! HTML::script('admin/global/plugins/sweetalert2/sweetalert2.min.js') !!}
<script>
    $("body").on('click','.ver-ejercicio', function () {
        var id = $(this).attr("data-id");
        window.location.href = "{{url('customer-app/rutinas/show/')}}" + '/' + id;
    });
</script>
@endsection