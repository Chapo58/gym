@extends('layouts.customer-app.basic')

@section('title')
    Improve | Ver Ejercicio
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Rutina de Ejercicios</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menú Principal</li>
                <li><a href="{{ route('customer-app.rutinas.index') }}">Rutina de Ejercicios</a></li>
                <li class="active">{{$ejercicio}}</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{$ejercicio}}</h3>
                @if($ejercicio->archivo_url)
                    <img src="{{url($ejercicio->archivo_url)}}" style="max-width:100%;">
                @endif
                @if($ejercicio->descripcion)
                    <hr>
                    {{$ejercicio->descripcion}}
                @endif
                <hr>
                <a href="{{ route('customer-app.rutinas.index') }}" class="btn btn-lg btn-info"><i class="fa fa-arrow-left"></i> Volver</a>
            </div>
        </div>
    </div>
@endsection