@extends('layouts.customer-app.basic')

@section('title')
    Improve | Rutina
@endsection

@section('CSS')
    {!! HTML::style('admin/global/plugins/sweetalert2/sweetalert2.min.css') !!}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Rutina de Ejercicios</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menú Principal</li>
                <li class="active">Rutina</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"><i class="fa fa-gears"></i> {{$customerValues->rutina}}</h3>
                    <p>{{$customerValues->rutina->descripcion}}</p>
                    <table class="table stylish-table">
                        <thead>
                            <tr>
                                <th>Ejercicio</th>
                                <th>Series</th>
                                <th>Repeticiones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($customerValues->rutina->ejercicios as $ejercicio)
                            <tr class="ver-ejercicio" data-id="{{$ejercicio->id}}">
                                <td> {{$ejercicio}} </td>
                                <td style="font-size:20px;font-weight:bold;">
                                    @if($ejercicio->pivot->series > 3)
                                        <span class="label label-warning font-weight-bold">
                                    @else
                                        <span class="label label-primary font-weight-bold">
                                    @endif
                                        {{$ejercicio->pivot->series}}
                                    </span>
                                </td>
                                <td style="font-size:20px;font-weight:bold;">
                                    @if($ejercicio->pivot->repeticiones > 10)
                                        <span class="label label-info font-weight-bold">
                                    @else
                                        <span class="label label-danger font-weight-bold">
                                    @endif
                                    {{$ejercicio->pivot->repeticiones}}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                <hr>
                <a href="{{url('customer-app/rutinas/rutinas')}}" class="btn btn-custom btn-block waves-effect waves-light compose-mail">Listado de Rutinas</a>
            </div>
        </div>
    </div>
@endsection

@section('JS')
    {!! HTML::script('admin/global/plugins/sweetalert2/sweetalert2.min.js') !!}
<script>
    $("body").on('click','.ver-ejercicio', function () {
        var id = $(this).attr("data-id");
        window.location.href = "{{url('customer-app/rutinas/show/')}}" + '/' + id;
    });
</script>
@endsection