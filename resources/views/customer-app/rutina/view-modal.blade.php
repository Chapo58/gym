<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myLargeModalLabel">{{$ejercicio}}</h4>
</div>
<div class="modal-body">
    <div class="row">
        @if($ejercicio->archivo_url)
        <div class="col-md-12">
            <img src="{{url($ejercicio->archivo_url)}}" style="max-width:100%;max-height:50%;">
        </div>
        @endif
        <div class="col-md-12">
            {{$ejercicio->descripcion}}
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>