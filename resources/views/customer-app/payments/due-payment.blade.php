@extends('layouts.customer-app.basic')

@section('title')
    Improve | Pagos Atrasados
@endsection

@section('CSS')
    {!! HTML::style('fitsigma_customer/bower_components/datatables/jquery.dataTables.min.css') !!}
@endsection

@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pagos Vencidos</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Menú Principal</li>
                <li>Pagos</li>
                <li class="active">Pagos Vencidos</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"><i class="fa {{$gymSettings->currency->symbol}}"></i> Pagos</h3>
                <p class="text-muted m-b-30"></p>
                <div class="table-responsive">
                    <table id="duePaymentTable" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Importe</th>
                            <th>Importe Restante</th>
                            <th>Descuento</th>
                            <th>Fecha</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('JS')
    {!! HTML::script('fitsigma_customer/bower_components/datatables/jquery.dataTables.min.js') !!}
    <script>
        var table = $('#duePaymentTable');
        table.dataTable({
            "responsive": true,
            "serverSide": true,
            "processing": true,
            "ajax": "{{ route('customer-app.payments.get-due-payment-data') }}",
            "aoColumns": [
                {'data': 'first_name', 'name': 'first_name'},
                {'data': 'purchase_amount', 'name': 'purchase_amount'},
                {'data': 'remaining_amount', 'name': 'remaining_amount'},
                {'data': 'discount', 'name': 'discount'},
                {'data': 'due_date', 'name': 'due_date'}
            ],
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No hay registros disponibles en la tabla",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty": "No se encontraron registros",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Mostrar _MENU_",
                "search": "Buscar:",
                "processing": "<i class='fa fa-spinner faa-spin animated'></i> Procesando",
                "zeroRecords": "No se encontraron coincidencias",
                "paginate": {
                    "previous":"Anterior",
                    "next": "Siguente",
                    "last": "Ultimo",
                    "first": "Primero"
                }
            }
        });
    </script>
@endsection