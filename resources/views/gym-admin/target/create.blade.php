@extends('layouts.gym-merchant.gymbasic')

@section('CSS')
    {!! HTML::style('admin/global/plugins/ladda/ladda-themeless.min.css') !!}
    {!! HTML::style('admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
    {!! HTML::style('admin/global/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}
@stop

@section('content')
    <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.target.index') }}">Objetivos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Agregar Objetivo</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">

                <div class="col-md-12">
                    <div class="m-heading-1 border-green m-bordered">
                        <h3>Objetivos</h3>
                        <p>La fijación de metas y objetivos es una herramienta efectiva para negocios progresivos, ya que proporciona un sentido de dirección y propósito.</p>
                        <p>Establece un objetivo para ti ahora.</p>
                    </div>
                </div>


                <div class="col-md-12">

                    <div class="portlet light portlet-fit">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-plus font-red"></i><span class="caption-subject font-red bold uppercase">Agregar Objetivo</span></div>
                        </div>
                        <div class="portlet-body">
                            {!! Form::open(['id'=>'addTargetForm','class'=>'ajax-form']) !!}

                            <div class="form-body">

                                <div class="form-group form-md-line-input ">
                                    <select  class="bs-select form-control" data-live-search="true" data-size="8" name="target_type" id="target_type">
                                        @foreach($target_type as $type)
                                            <option value="{{$type->id}}">{{ucfirst($type->type)}}</option>
                                        @endforeach
                                    </select>
                                    <label for="title">Tipo de Objetivo</label>
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group left-addon right-addon">
                                        <span class="input-group-addon"><i class="icon-tag"></i></span>
                                        <input type="text" class="form-control" name="title" id="title">
                                        <span class="help-block">Ingresa el nombre del objetivo</span>
                                        <label for="price">Nombre del Objetivo</label>
                                    </div>
                                </div>

                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group left-addon right-addon">
                                        <span class="input-group-addon"><i class="icon-bag"></i></span>
                                        <input type="number" min="0" class="form-control" name="value" id="value">
                                        <span class="help-block">Ingresa el valor del objetivo</span>
                                        <label for="price">Valor del Objetivo</label>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input ">
                                            <div class="input-icon">
                                                <input type="text" readonly class="form-control date-picker" placeholder="Selecciona la fecha de inicio" name="start_date" id="start_date" >
                                                <label for="form_control_1 ">Fecha de Inicio</label>
                                                <span class="help-block">Fecha de Inicio</span>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-md-line-input">
                                            <div class="input-icon">
                                                <input type="text" readonly class="form-control date-picker" placeholder="Selecciona la fecha de finalización" name="date" id="date" >
                                                <label for="form_control_1">Fecha de Finalización</label>
                                                <span class="help-block">Fecha de Finalización</span>
                                                <i class="icon-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="form-actions" style="margin-top: 70px">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn dark mt-ladda-btn ladda-button" data-style="zoom-in" id="save-form">
                                            <span class="ladda-label"><i class="fa fa-save"></i> GUARDAR</span>
                                        </button>
                                        <button type="reset" class="btn default">Reiniciar</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>


                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
@stop
@section('footer')
    {!! HTML::script('admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/spin.min.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/ladda.min.js') !!}
    {!! HTML::script('admin/pages/scripts/ui-buttons.min.js') !!}
    {!! HTML::script('admin/global/plugins/bootstrap-select/js/bootstrap-select.min.js') !!}
    {!! HTML::script('admin/pages/scripts/components-bootstrap-select.min.js') !!}
    <script>
        $('#start_date').datepicker({
            autoclose: true,
        }).on('changeDate', function(){
            $('#date').datepicker('setStartDate', new Date($(this).val()));
        });

        $('#date').datepicker({
            autoclose: true,
        }).on('changeDate', function(){
            $('#start_date').datepicker('setEndDate', new Date($(this).val()));
        });
    </script>
    <script>
        $('#save-form').click(function(){
            $.easyAjax({
                url:'{{route('gym-admin.target.store')}}',
                container:'#addTargetForm',
                type:'POST',
                data:$('#addTargetForm').serialize(),
                formReset:true
            });
        });
    </script>
@stop
