<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <span class="caption-subject font-red-sunglo bold uppercase">Cambiar Estado</span>
</div>
<div class="modal-body">
    {!! Form::open(['id'=>'changeStatusForm','class'=>'ajax-form']) !!}
    <div class="row">
        <div class="col-md-12">

            <div class="form-group">
                <label class="control-label">Estado</label>
                <select class="form-control" name="status">
                    <option value="pending" @if($enquiry->status == 'pending')selected @endif>Pendiente</option>
                    <option value="in_process" @if($enquiry->status == 'in_process')selected @endif>En Progreso</option>
                    <option value="resolved" @if($enquiry->status == 'resolved')selected @endif>Terminado</option>
                </select>
            </div>
            <input type="hidden" name="id" value="{{$enquiry->id}}">
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:;" class="btn blue" id="statusChange" >Cambiar</a>
    </div>
    {!! Form::close() !!}
</div>

<script>
    $('#statusChange').click(function(){
        $.easyAjax({
            url: '{{route('gym-admin.enquiry.status')}}',
            container:'#changeStatusForm',
            type: "POST",
            data:$('#changeStatusForm').serialize()
        })
    });
</script>
