@extends('layouts.gym-merchant.gymbasic')

@section('content')
    <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.galeria.index') }}">Galeria</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Ver Archivo</span>
            </li>
        </ul>

        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-eye font-red"></i>
                                <span class="caption-subject font-red bold uppercase">{{ $archivo }}</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Nombre</div>
                                    <div class="col-md-3">{{ $archivo->nombre }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Imagen</div>
                                    <div class="col-md-3">
                                        <img src="{{ $archivo->archivo_url  }}" width="200" style="border: 2px solid lightslategrey;">
                                    </div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Slider</div>
                                    <div class="col-md-3">
                                        @if($archivo->slider == 'si')
                                            <span class="label label-success font-weight-bold">
                                        @else
                                            <span class="label label-danger font-weight-bold">
                                        @endif
                                        {{ strtoupper($archivo->slider) }}</span>
                                    </div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Creado el</div>
                                    <div class="col-md-3">{{ $archivo->created_at->format('d/m/Y H:m') }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                                    <div class="col-md-3">{{ $archivo->updated_at->format('d/m/Y H:m') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
