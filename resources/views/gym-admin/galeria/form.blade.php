<div class="form-body">
    <div class="form-group form-md-line-input">
        <label class="col-md-2" for="form_control_1">Nombre del Archivo</label>
        <div class="col-md-6">
            <div class="input-icon right">
                <input type="text" class="form-control" value="{{isset($archivo) ? $archivo->nombre : ''}}" placeholder="Nombre del archivo" name="nombre" id="nombre">
                <div class="form-control-focus"> </div>
                <span class="help-block">Ingresar nombre del archivo</span>
                <i class="icon-user"></i>
            </div>
        </div>
    </div>
    <div class="form-group form-md-line-input" align="center">
        <label class="control-label">¿Colocar imagen en el Slider de la App?</label>
        <div class="form-md-radios" align="center">
            <div class="md-radio-inline">
                <div class="md-radio">
                    <input type="radio" id="slider-yes" {{(isset($archivo) && $archivo->slider == 'si') ? 'checked' : ''}} name="slider" value="Si" class="md-radiobtn">
                    <label for="slider-yes">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span> Si </label>
                </div>
                <div class="md-radio">
                    <input type="radio" id="slider-no" {{(!isset($archivo) || $archivo->slider == 'no') ? 'checked' : ''}} name="slider" class="md-radiobtn" value="No" >
                    <label for="slider-no">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span> No</label>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="form-md-line-input">
        <div class="col-md-9">
            <div class="form-group">
                <label>Seleccionar Imagen o Archivo</label>
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="input-group image-preview">
                            <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                            <span class="input-group-btn">
                                <!-- image-preview-clear button -->
                                <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                  <span class="glyphicon glyphicon-remove"></span> Limpiar
                                </button>
                                <!-- image-preview-input -->
                                <div class="btn btn-default image-preview-input">
                                  <span class="glyphicon glyphicon-folder-open"></span>
                                  <span class="image-preview-input-title">Buscar</span>
                                  <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif">
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            {!! Form::label('imagen_url', 'Imagen Actual') !!}
            <img src="{{ isset($archivo->archivo_url) ? $archivo->archivo_url : '/admin/global/img/no-image.png' }}" width="200" style="border: 2px solid lightslategrey;">
        </div>
    </div>
    <hr>
</div>

<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <a href="javascript:;" class="btn green" id="enviarFormulario">Aceptar</a>
            <a href="{{ route('gym-admin.galeria.index') }}" class="btn default">Cancelar</a>
        </div>
    </div>
</div>
