@extends('layouts.gym-merchant.gymbasic')

@section('CSS')
    {!! HTML::style('admin/global/plugins/datatables/datatables.min.css') !!}
    {!! HTML::style('admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
@stop

@section('content')
    <div class="container-fluid">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('gym-admin.dashboard.index')}}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Gestionar Sucursales</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="fa fa-cogs font-red"></i>
                                <span class="caption-subject font-red bold uppercase"> Sucursales</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">

                                        <a id="sample_editable_1_new" href="{{route('gym-admin.superadmin.branch')}}" class="btn sbold dark"> Agregar Nuevo
                                            <i class="fa fa-plus"></i>
                                        </a>

                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column table-100" id="manage-branches">
                                <thead>
                                <tr>
                                    <th class="max-desktop"> Nombre Sucursal </th>
                                    <th class="desktop"> Acciones </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>

    {{--Modal Start--}}
    <div class="modal fade bs-modal-md in" id="branchModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" id="modal-data-application">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
                </div>
                <div class="modal-body">
                    Cargando...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="deleteBranch" class="btn btn-danger">Eliminar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{--Modal End--}}
@stop

@section('footer')
    {!! HTML::script('admin/global/scripts/datatable.js') !!}
    {!! HTML::script('admin/pages/scripts/table-datatables-managed.js') !!}
    {!! HTML::script('admin/global/plugins/datatables/datatables.min.js') !!}
    {!! HTML::script('admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
<script>
    var table = $('#manage-branches');
    table.dataTable({
        responsive: true,
        "serverSide": true,
        "processing": true,
        "ajax": "{{ route('gym-admin.superadmin.getData') }}",
        "aoColumns": [
            {'data': 'title', 'name': 'title', 'searchable': true},
            {'data': 'actions', 'name': 'actions'}
        ],
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No hay registros disponibles en la tabla",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty": "No se encontraron registros",
            "infoFiltered": "(filtered1 from _MAX_ total records)",
            "lengthMenu": "Mostrar _MENU_",
            "search": "Buscar:",
            "processing": "<i class='fa fa-spinner faa-spin animated'></i> Procesando",
            "zeroRecords": "No se encontraron coincidencias",
            "paginate": {
                "previous":"Anterior",
                "next": "Siguente",
                "last": "Ultimo",
                "first": "Primero"
            }
        },

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

        "columnDefs": [ {
            "targets": 0,
            "orderable": false,
            "searchable": false
        }],

        "lengthMenu": [
            [5, 15, 20, -1],
            [5, 15, 20, "Todos"] // change per page values here
        ],
        // set the initial value
        "pageLength": 5,
        "pagingType": "bootstrap_full_number",
        "columnDefs": [{  // set default column settings
            'orderable': false,
            'targets': [0]
        }, {
            "searchable": false,
            "targets": [0]
        }],
        "order": [
            [1, "asc"]
        ] // set first column as a default sort by asc
    });

    function deleteModal(id) {
        var url_modal = '{{ route('gym-admin.superadmin.destroy', [':id']) }}';
        var url = url_modal.replace(':id',id);
        $('#modelHeading').html('Eliminar Sucursal');
        var body = '¿Desea eliminar esta sucursal?<br>' +
            '<br><label class="label label-danger">NOTA:</label>  Usuarios, clientes y toda la informacion relacionada con esta sucursal tambien sera eliminada y no podra recuperarse.';
        $('.modal-body').html(body);
        $('#branchModal').modal("show");
        $('#deleteBranch').on('click', function () {
            $.easyAjax({
                url: url,
                type: 'DELETE',
                data: {'_token': "{{ csrf_token() }}"},
                success: function (response) {
                    if(response.status == "success") {
                        $('#branchModal').modal('hide');
                        table._fnDraw();
                        window.location.reload();
                    }
                }
            })
        })
    }
</script>
@stop