@extends('layouts.gym-merchant.gymbasic')

@section('CSS')
    {!! HTML::style('admin/global/plugins/ladda/ladda-themeless.min.css') !!}
    {!! HTML::style('admin/global/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}
    {!! HTML::style('admin/global/plugins/typeahead/typeahead.css') !!}
@stop

@section('content')
   <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.rutinas.index') }}">Rutinas</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Agregar Rutina</span>
            </li>
        </ul>

        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12 col-xs-12">

                    <div class="portlet light portlet-fit">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-plus font-red"></i>
                                <span class="caption-subject font-red bold uppercase"> Crear Rutina</span>
                            </div>
                        </div>
                        <div class="portlet-body">

    {!! Form::open(['id'=>'rutinas_data', 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
        @include ('gym-admin.rutinas.form')
    {!! Form::close() !!}

                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer')
    {!! HTML::script('admin/global/plugins/ladda/spin.min.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/ladda.min.js') !!}
    {!! HTML::script('admin/pages/scripts/ui-buttons.min.js') !!}
    {!! HTML::script('admin/global/plugins/bootstrap-select/js/bootstrap-select.min.js') !!}
    {!! HTML::script('admin/pages/scripts/components-bootstrap-select.min.js') !!}

    <script>
        $('#enviarFormulario').click(function(){
            $.easyAjax({
                url: '{{route('gym-admin.rutinas.store')}}',
                container:'#rutinas_data',
                type: "POST",
                file:true,
                formReset:true
            });
        });

        $('#add-item').click(function () {
            var numero_orden = $('table#tabla-detalles-ejercicios tbody tr').length + 1;
            $("#tabla-detalles-ejercicios").append('<tr>'

                + '<td>'
                + '<select class="bs-select form-control item_ejercicios" data-live-search="true" data-size="8" name="ejercicios['+numero_orden+'][ejercicio_id]" required>'
                + '<option value="">Seleccionar Ejercicio</option>'
                @foreach($ejercicios as $ejercicio)
                    + '<option value="{{$ejercicio->id}}">{{$ejercicio}}</option>'
                @endforeach
                + '</select>'
                + '</td>'

                + '<td>'
                + '<input type="number" class="form-control item_series" name="ejercicios['+numero_orden+'][series]" placeholder="Series" required>'
                + '</td>'

                + '<td>'
                + '<input type="number" class="form-control item_repeticiones" name="ejercicios['+numero_orden+'][repeticiones]" placeholder="Cant. Repeticiones" required>'
                + '</td>'

                + '<td style="width:5%;">'
                + '<button type="button" class="btn remove-item btn-icon-only red"><i class="fa fa-remove"></i></button>'
                + '</td>'

                +'</tr>');

                $('.item_ejercicios').selectpicker({});
        });

        $(document).on('click', '.remove-item', function () {
            var fila = $(this).parent().parent();

            fila.hide(1000, function(){
                fila.find('td select.item_ejercicios').val(0);
                fila.find('td input.item_series').val(0);
                fila.find('td input.item_repeticiones').val(0);
            });
        });

    </script>
@stop
