<div class="form-body">
    <div class="form-group form-md-line-input">
        <label class="col-md-2" for="form_control_1">Nombre de la Rutina</label>
        <div class="col-md-6">
            <div class="input-icon right">
                <input type="text" class="form-control" value="{{isset($rutina) ? $rutina->nombre : ''}}" placeholder="Nombre de la rutina" name="nombre" id="nombre">
                <div class="form-control-focus"> </div>
                <span class="help-block">Ingresar nombre de la rutina</span>
                <i class="icon-user"></i>
            </div>
        </div>
    </div>
    <div class="form-group form-md-line-input col-md-8">
        <label for="form_control_1">Descripción</label>
        <textarea name="descripcion" class="form-control" id="descripcion" cols="30" rows="3">
            {{isset($rutina) ? $rutina->descripcion : ''}}
        </textarea>
        <div class="form-control-focus"></div>
    </div>
    <br>

    <table class="table table-striped table-bordered table-hover" id="tabla-detalles-ejercicios">
        <thead>
            <tr>
                <th class="bg-dark bg-font-dark">Ejercicio</th>
                <th class="bg-dark bg-font-dark">Series</th>
                <th class="bg-dark bg-font-dark">Repeticiones</th>
                <th class="bg-dark bg-font-dark"></th>
            </tr>
        </thead>
        <tbody>
            @if(isset($rutina) && !$rutina->ejercicios->isEmpty())
                <?php $contador = 0; ?>
                @foreach($rutina->ejercicios as $item)
                    <tr>
                        <td>
                            <select  class="bs-select form-control item_ejercicios" data-live-search="true" data-size="8" name="ejercicios[{{$contador}}][ejercicio_id]" required>
                                <option value="">Seleccionar Ejercicio</option>
                                @foreach($ejercicios as $ejercicio)
                                    <option value="{{$ejercicio->id}}" {{($item->id == $ejercicio->id) ? 'selected' : ''}}>{{$ejercicio}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input type="number" class="form-control item_series" name="ejercicios[{{$contador}}][series]" value="{{$item->pivot->series}}" placeholder="Series" required>
                        </td>
                        <td>
                            <input type="number" class="form-control item_repeticiones" name="ejercicios[{{$contador}}][repeticiones]" value="{{$item->pivot->repeticiones}}" placeholder="Cant. Repeticiones" required>
                        </td>
                        <td style="width:5%;">
                            <button type="button" class="btn remove-item btn-icon-only red"><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>
                    <?php $contador++; ?>
                @endforeach
            @else
            <tr>
                <td>
                    <select  class="bs-select form-control item_ejercicios" data-live-search="true" data-size="8" name="ejercicios[0][ejercicio_id]" required>
                        <option value="">Seleccionar Ejercicio</option>
                        @foreach($ejercicios as $ejercicio)
                            <option value="{{$ejercicio->id}}">{{$ejercicio}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" class="form-control item_repeticiones" name="ejercicios[0][series]"  placeholder="Series" required>
                </td>
                <td>
                    <input type="number" class="form-control item_repeticiones" name="ejercicios[0][repeticiones]"  placeholder="Cant. Repeticiones" required>
                </td>
                <td style="width:5%;">
                    <button type="button" class="btn remove-item btn-icon-only red"><i class="fa fa-remove"></i></button>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
    <div class="col-xs-12 margin-top-5">
        <button type="button" class="btn blue" id="add-item"><i class="fa fa-plus"></i> Agregar Item
        </button>
    </div>
    <hr>
</div>

<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <a href="javascript:;" class="btn green" id="enviarFormulario">Aceptar</a>
            <a href="{{ route('gym-admin.rutinas.index') }}" class="btn default">Cancelar</a>
        </div>
    </div>
</div>
