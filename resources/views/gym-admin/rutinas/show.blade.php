@extends('layouts.gym-merchant.gymbasic')

@section('content')
    <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.rutinas.index') }}">Rutinas</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Ver Rutina</span>
            </li>
        </ul>

        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-eye font-red"></i>
                                <span class="caption-subject font-red bold uppercase">{{ $rutina }}</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Nombre</div>
                                    <div class="col-md-3">{{ $rutina->nombre }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Descripción</div>
                                    <div class="col-md-3">{{ $rutina->descripcion }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Creado el</div>
                                    <div class="col-md-3">{{ $rutina->created_at->format('d/m/Y H:m') }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                                    <div class="col-md-3">{{ $rutina->updated_at->format('d/m/Y H:m') }}</div>
                                </div>
                                @if(!$rutina->ejercicios->isEmpty())
                                <hr>
                                    <table class="table table-striped table-bordered table-hover" id="tabla-detalles-ejercicios">
                                        <thead>
                                        <tr>
                                            <th class="bg-dark bg-font-dark">Ejercicio</th>
                                            <th class="bg-dark bg-font-dark">Series</th>
                                            <th class="bg-dark bg-font-dark">Repeticiones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rutina->ejercicios as $ejercicio)
                                                <tr>
                                                    <td> {{$ejercicio}} </td>
                                                    <td> {{$ejercicio->pivot->series}} </td>
                                                    <td> {{$ejercicio->pivot->repeticiones}} </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
