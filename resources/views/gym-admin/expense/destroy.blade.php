<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <span class="caption-subject font-red-sunglo bold uppercase">Eliminar Ejercicio</span>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <p> Desea eliminar el ejercicio {{$ejercicio->item_name}} ?</p>
        </div>
    </div>
</div>
</div>
<div class="modal-footer">
    <a href="javascript:;" class="btn blue" id="removeEjercicio" >Eliminar</a>
</div>

<script>
    $('#removeEjercicio').click(function(){
        $.easyAjax({
            url: '{{route('gym-admin.ejercicio.destroy', $ejercicio->id)}}',
            container:'.modal-body',
            type: "DELETE"
        })
    });
</script>