@extends('layouts.gym-merchant.gymbasic')

@section('CSS')
    {!! HTML::style('admin/global/plugins/ladda/ladda-themeless.min.css') !!}
    {!! HTML::style('admin/global/plugins/bootstrap-select/css/bootstrap-select.min.css') !!}
@stop

@section('content')
    <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.membership-payment.index') }}">Pagos</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Agregar Pago</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">

            @if($completedItems  < $completedItemsRequired)
                {{-- Account setup progress start --}}

                <div class="row">

                    <div class="col-md-12">
                        <div class="portlet box dark">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-speedometer font-white"></i>
								<span class="caption-subject  font-white ">
								Progreso de configuración de la cuenta </span>
                                    <span class="caption-helper">{{ round($completedItems*(100/$completedItemsRequired),1) }}% COMPLETADO</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="col-md-12">
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{ ($completedItems*(100/$completedItemsRequired)) }}%">
									<span class="sr-only">
									{{ ($completedItems*(100/$completedItemsRequired)) }}% Completado </span>
                                        </div>
                                    </div>
                                </div>

                                @if(trim($user->first_name) == "" || trim($user->first_name) == "")
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ route('gym-admin.profile.index') }}">
                                            Actualiza tu nombre y apellido


                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>

                                @elseif(trim($user->mobile) == "")
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ route('gym-admin.profile.index') }}">
                                            Actualiza tu número de móvil

                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>

                                @elseif(count($memberships) == 0)
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ URL::route('gym-admin.membership.create') }}">
                                            Agregar Membresía

                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>

                                @elseif(count($clients) == 0)
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ route('gym-admin.client.create') }}">
                                            Agregar Primer Cliente

                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>



                                @elseif(count($subscriptions) == 0)
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ route('gym-admin.client-purchase.create') }}">
                                            Agregar Suscripción

                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>

                                @elseif(count($payments) == 0)
                                    <div class="col-md-12">
                                        <strong>Siguiente Paso:</strong>
                                        <a href="{{ route('gym-admin.membership-payment.create') }}">
                                            Agregar Pago

                                            <i class="fa fa-arrow-right"></i>
                                        </a>

                                    </div>

                                @endif

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                </div>
                {{-- Account setup progress end --}}
            @endif


            <div class="row">
                <div class="col-md-12 col-xs-12">

                    <div class="portlet light portlet-fit">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-plus font-red"></i><span class="caption-subject font-red bold uppercase">Agregar Pago</span></div>


                        </div>
                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            {!! Form::open(['id'=>'storePayments','class'=>'ajax-form','method'=>'POST']) !!}
                            <div class="form-body">

                                <div class="form-group form-md-line-input ">
                                        <select  class="bs-select form-control" data-live-search="true" data-size="8" name="client" id="client">
                                            <option value="">Seleccionar Cliente</option>
                                            @foreach($clients as $client)
                                                <option value="{{$client->id}}">{{$client->first_name}}&nbsp;{{$client->last_name}}</option>
                                            @endforeach
                                        </select>
                                        <label for="title">Nombre Cliente <span class="required" aria-required="true"> * </span></label>
                                        <span class="help-block"></span>
                                    </div>

                                <div id="payment_for_area">

                                </div>

                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group left-addon right-addon">
                                        <span class="input-group-addon"><i class="fa {{ $gymSettings->currency->symbol }}"></i></span>
                                        <input type="number" min="0" class="form-control" name="payment_amount" id="payment_amount">
                                        <span class="help-block">Ingresar Importe</span>
                                        <span class="input-group-addon">.00</span>
                                        <label for="price">Importe del Pago <span class="required" aria-required="true"> * </span></label>
                                    </div>
                                </div>
                                <div id="remaining_div">
                                <div class="form-group form-md-line-input form-md-floating-label">
                                    <div class="input-group left-addon right-addon">
                                        <span class="input-group-addon"><i class="fa {{ $gymSettings->currency->symbol }}"></i></span>
                                        <input disabled type="number" min="0" class="form-control" name="remaining_amount" id="remaining_amount">
                                        <input disabled type="hidden" class="form-control" name="remaining_amount_store" id="remaining_amount_store">
                                        <span class="input-group-addon">.00</span>
                                        <label for="price">Importe Pendiente</label>
                                    </div>
                                </div></div>

                                <div class="form-group form-md-line-input">
                                    <div class="form-group form-md-radios">
                                        <label>¿Forma de Pago? <span class="required" aria-required="true"> * </span></label>
                                        <div class="md-radio-inline">
                                            <div class="md-radio">
                                                <input type="radio" value="cash" id="cash_radio" name="payment_source" class="md-radiobtn">
                                                <label for="cash_radio">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> <i class="fa fa-money"></i> Efectivo </label>
                                            </div>
                                            <div class="md-radio ">
                                                <input type="radio" value="credit_card" id="credit_card_radio" name="payment_source" class="md-radiobtn" >
                                                <label for="credit_card_radio">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> <i class="fa fa-credit-card"></i> Tarjeta de Credito </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" value="debit_card" id="debit_card_radio" name="payment_source" class="md-radiobtn">
                                                <label for="debit_card_radio">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> <i class="fa fa-cc-visa"></i> Tarjeta de Debito </label>
                                            </div>
                                            <div class="md-radio">
                                                <input type="radio" value="net_banking" id="net_banking_radio" name="payment_source" class="md-radiobtn">
                                                <label for="net_banking_radio">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span> <i class="fa fa-internet-explorer"></i> Banco </label>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>


                                <div class="form-group form-md-line-input ">
                                    <div class="input-group left-addon right-addon">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control date-picker" readonly name="payment_date" id="payment_date" value="{{ \Carbon\Carbon::now('America/Argentina/Cordoba')->format('m/d/Y') }}">
                                        <label for="payment_date">Fecha de Pago</label>
                                    </div>
                                </div>

                                <div id="onlyMembership">
                                    <div class="form-group form-md-line-input">
                                        <div class="form-group form-md-radios">
                                            <label>Más pagos requeridos</label>
                                            <div class="md-radio-inline">
                                                <div class="md-radio">
                                                    <input type="radio" value="yes" id="yes_radio" name="payment_required" class="md-radiobtn" checked>
                                                    <label for="yes_radio">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> Si </label>
                                                </div>
                                                <div class="md-radio ">
                                                    <input type="radio" value="no" id="no_radio" name="payment_required" class="md-radiobtn" >
                                                    <label for="no_radio">
                                                        <span></span>
                                                        <span class="check"></span>
                                                        <span class="box"></span> No </label>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="help-block"></span>
                                    </div>

                                    <div class="form-group form-md-line-input " id="next_payment_div" style="display: none">
                                        <div class="input-group left-addon right-addon">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control date-picker" readonly name="next_payment_date" id="next_payment_date">
                                            <label for="payment_date">Siguente Fecha de Pago</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-md-line-input ">
                                            <div class="input-icon">
                                                <input type="text" class="form-control" placeholder="Observaciones" name="remark" id="remark">
                                                <label for="form_control_1">Observación</label>
                                                <span class="help-block">Agregar observaciones del pago</span>
                                                <i class="fa fa-pencil"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions" style="margin-top: 70px">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn dark mt-ladda-btn ladda-button" data-style="zoom-in" id="save-form">
                                            <span class="ladda-label"><i class="fa fa-save"></i> GUARDAR</span>
                                        </button>
                                        <button type="reset" class="btn default">Reiniciar</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                                    <!-- END FORM-->
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>

    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@stop

@section('footer')

    {!! HTML::script('admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
    {!! HTML::style('admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}
    {!! HTML::script('admin/global/plugins/ladda/spin.min.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/ladda.min.js') !!}
    {!! HTML::script('admin/pages/scripts/ui-buttons.min.js') !!}
    {!! HTML::script('admin/global/plugins/bootstrap-select/js/bootstrap-select.min.js') !!}
    {!! HTML::script('admin/pages/scripts/components-bootstrap-select.min.js') !!}
<script>

    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        orientation: "left",
        autoclose: true
    });
    $("document").ready(function(){
        $('#onlyMembership').css('display','none');
        $('#payment_for_area').html('');
    });

    $("#payment_type").change(function(){
        var type = $("#payment_type option:selected").val();

        if(type != 'membership'){
            $('#onlyMembership').css('display','none');
            $('#payment_for_area').html('');
            $("#remaining_div").css('display','none');

            return false;
        }
        else if($("#remaining_amount").val() == '')
        {
            $('#onlyMembership').css('display','none');
            $('#payment_for_area').html('');

            return false;
        }
        else {
            $("#remaining_div").css('display','block');
            $('#onlyMembership').css('display','block');
            $('#payment_for_area').html('');

            return false;
        }

    });

    $("#client").on("change",function(){
        var amount = 0;
        var clientId = $("#client").val();
        var url = '{{route('gym-admin.gympurchase.clientPayment',[':id'])}}';
        url = url.replace(':id',clientId);
        remaining(amount,url);
    });

    $('#storePayments').on('change', '#purchase_id', function () {
        var purchaseId = $(this).val();
        var url = '{{route('gym-admin.gympurchase.remainingPayment',[':id'])}}';
        url = url.replace(':id',purchaseId);
        $.easyAjax({
            url : url,
            type:'GET',
            data: { purchaseId: purchaseId},
            success:function(response)
            {
                $('#remaining_amount').val(response);
                $('#remaining_amount_store').val(response);
            }
        })
    });

    $('#payment_amount').on("input", function() {
        var amount = this.value;
        var clientId = $("#client").val();
        var remaining = $("#remaining_amount_store").val()-amount;
        $("#remaining_amount").addClass("edited");
        if(parseFloat(remaining) < 0){
            remaining = 0;
        }
        $("#remaining_amount").val(remaining);
       // remaining(amount,url);

    });

    function remaining(amount,url) {
        $.easyAjax({
            url : url,
            type:'GET',
            data: { amount:amount},
            success:function(response)
            {
                $("#remaining_amount").addClass("edited");
                if(parseFloat(response.payment.diff) < 0){
                    response.payment.diff = 0;
                }
                $("#remaining_amount").val(response.payment.diff);
                $("#remaining_amount_store").val(response.payment.diff);
                if(response.payment.diff >= 0)
                {
                    $('#onlyMembership').css('display','block');
                    $('#next_payment_div').css('display','block');
                    $("#next_payment_date").datepicker( "setDate" , '+'+response.payment.emi_days+'d' );
                    //$("#next_payment_date").val('');
                }
            }
        })
    }


    $("input[name='payment_required']").change(function(){
        var type = $("input[name='payment_required']:checked").val();
        var remainingAmount = $('#remaining_amount').val();
        if(type == 'yes')
        {
            if(remainingAmount == 0) {
                $('.modal-title').text('Nota');
                $('.modal-body').text('Has marcado que existen pagos restantes.');
                $('#basic').modal('show');
            }
            $('#next_payment_div').css('display','block');
        }else {
            if(remainingAmount > 0) {
                $('.modal-title').text('Nota');
                $('.modal-body').text('Has marcado que no existen pagos restantes.');
                $('#basic').modal('show');
            }
            $('#next_payment_div').css('display','none');
        }
    });



    $('#client').change(function () {
        var clientId = $(this).val();
        if(clientId == "")return false;
            var url = '{{route('gym-admin.gympurchase.clientPurchases',[':id'])}}';
            url = url.replace(':id',clientId);

            $.easyAjax({
                url: url,
                type: 'GET',
                data: {clientID: clientId },
                success: function(response){
                    $('#payment_for_area').html(response.data);
                }
            })
    });
</script>
    <script>
        $('#save-form').click(function(){
            var type = $("input[name='payment_required']:checked").val();

                $.easyAjax({
                    url: '{{route('gym-admin.membership-payment.store')}}',
                    container: '#storePayments',
                    type: "POST",
                    data: $('#storePayments').serialize(),
                    success: function (responce) {
                        if (responce.status == 'success') {
                            clear_form_elements('storePayments')
                        }
                    }
                })
        });

        $('#payment_amount').keyup(function(){
            var remainingAmount = $('#remaining_amount_store').val();
            var total = remainingAmount - $(this).val();
            if(total > 0) {
                $('#yes_radio').prop("checked", true);
            } else {
                $('#no_radio').prop("checked", true);
            }
        });

    </script>
@stop