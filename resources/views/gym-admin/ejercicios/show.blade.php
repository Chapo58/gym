@extends('layouts.gym-merchant.gymbasic')

@section('content')
    <div class="container-fluid"  >
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('gym-admin.dashboard.index') }}">Inicio</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('gym-admin.ejercicios.index') }}">Ejercicios</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Ver Ejercicio</span>
            </li>
        </ul>

        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <i class="icon-eye font-red"></i>
                                <span class="caption-subject font-red bold uppercase">{{ $ejercicio }}</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Nombre</div>
                                    <div class="col-md-3">{{ $ejercicio->nombre }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Imagen</div>
                                    <div class="col-md-3">
                                        <img src="{{ isset($ejercicio->archivo_url) ? $ejercicio->archivo_url : '/admin/global/img/no-image.png' }}" width="200" style="border: 2px solid lightslategrey;">
                                    </div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Descripción</div>
                                    <div class="col-md-3">{{ $ejercicio->descripcion }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Creado el</div>
                                    <div class="col-md-3">{{ $ejercicio->created_at->format('d/m/Y H:m') }}</div>
                                </div><hr>
                                <div class="row">
                                    <div class="col-md-3 font-weight-bold">Ultima Modificación</div>
                                    <div class="col-md-3">{{ $ejercicio->updated_at->format('d/m/Y H:m') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
