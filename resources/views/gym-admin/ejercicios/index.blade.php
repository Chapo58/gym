@extends('layouts.gym-merchant.gymbasic')

@section('CSS')
    {!! HTML::style('admin/global/plugins/ladda/ladda-themeless.min.css') !!}
    {!! HTML::style('admin/global/plugins/datatables/datatables.min.css') !!}
    {!! HTML::style('admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}
    {!! HTML::style('admin/global/plugins/sweetalert2/sweetalert2.min.css') !!}
@stop

@section('content')
<div class="container-fluid">
    <!-- BEGIN PAGE BREADCRUMBS -->
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{route('gym-admin.dashboard.index')}}">Inicio</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Ejercicios</span>
        </li>
    </ul>
    <!-- END PAGE BREADCRUMBS -->
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="page-content-inner">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="fa fa-cog font-red"></i>
                            <span class="caption-subject font-red bold uppercase"> Ejercicios</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">

                                    <a id="sample_editable_1_new" href="{{route('gym-admin.ejercicios.create')}}" class="btn sbold dark"> Agregar Nuevo
                                        <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" style="width: 100%" id="ejercicios">
                            <thead>
                            <tr>
                                <th class="desktop">Imagen</th>
                                <th class="desktop">Nombre</th>
                                <th class="desktop">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($ejercicios as $item)
                                <tr id="{{$item->id}}">
                                    <td>
                                        <img src="{{ isset($item->archivo_url) ? $item->archivo_url : '/admin/global/img/no-image.png' }}" width="100" style="border: 2px solid lightslategrey;">
                                    </td>
                                    <td>{{ $item->nombre }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn blue btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-gears"></i> <span class="hidden-xs">Acciones</span>
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li>
                                                    <a href="{{ url('/gym-admin/ejercicios/' . $item->id) }}"> <i class="fa fa-eye"></i> Ver</a>
                                                </li>
                                                <li>
                                                    <a href="{{ url('/gym-admin/ejercicios/' . $item->id . '/edit') }}"> <i class="fa fa-edit"></i> Editar</a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" onClick="deleteModal({{$item->id}})"> <i class="fa fa-trash"></i> Eliminar</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>
{{--Modal Start--}}

<div class="modal fade bs-modal-md in" id="gymExpenseModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="modelHeading"></span>
            </div>
            <div class="modal-body">
                Cargando...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn blue">Guardar cambios</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--End Modal--}}

@stop

@section('footer')
    {!! HTML::script('admin/global/scripts/datatable.js') !!}
    {!! HTML::script('admin/pages/scripts/table-datatables-managed.js') !!}
    {!! HTML::script('admin/global/plugins/datatables/datatables.min.js') !!}
    {!! HTML::script('admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/spin.min.js') !!}
    {!! HTML::script('admin/global/plugins/ladda/ladda.min.js') !!}
    {!! HTML::script('admin/global/plugins/sweetalert2/sweetalert2.min.js') !!}
    {!! HTML::script('admin/pages/scripts/ui-buttons.min.js') !!}

    <script>

        var table = $('#ejercicios');

        table.dataTable({
            responsive: true,
            "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "Todos"]
            ],
            "pageLength": 5,
            "pagingType": "bootstrap_full_number",
        });


        function deleteModal(id) {
            swal({
                title: "Eliminar Ejercicio",
                text: "¿Realmente desea eliminar este registro?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#ff8726',
                confirmButtonText: "Eliminar"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/gym-admin/ejercicios/'+id,
                        data: {"_method" : 'DELETE'},
                        success: function (msg) {
                            $("#" + id).hide(1);
                            swal("Eliminado!", "El registro ha sido correctamente eliminado.", "success");
                        }
                    });
                }
            });
        }
    </script>
@stop
