<!DOCTYPE html>
<!--[if IE 7 ]><body class="ie ie7"><![endif]-->
<!--[if IE 8 ]><body class="ie ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie ie9"><![endif]-->
<html class='no-js' lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
	<title>Improve.fit</title>
	<meta content="" name="keywords">
	<meta content="" name="description">

	<link type="image/x-icon" href="{{ asset('favicon/android-icon-192x192.png') }}" rel="shortcut icon">
	<link rel="stylesheet" href="{{ asset('landing/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/font-awesome.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/main.css') }}">
	<link rel="stylesheet" href="{{ asset('landing/css/responsive.css') }}">


</head>
<body>
<!--===========================-->
<!--==========Header===========-->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>
</div>

<div class="main-holder">
<header class='main-wrapper header'>
	<div class="container apex">
		<div class="row">

			<nav class="navbar header-navbar" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<div class="logo navbar-brand">
						<a href="#" title="Advisa"></a>
					</div>
		      <button class='toggle-slide-left visible-xs collapsed navbar-toggle' type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-bars"></i></button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<div class="navbar-right">
							<nav class='nav-menu navbar-left main-nav trig-mob slide-menu-left'>
								<ul class='list-unstyled'>
									<li>
										<a href="#" data-scroll="features">
											<div class="inside">
												<div class="backside"> Caracteristicas </div>
												<div class="frontside"> Caracteristicas </div>
											</div>
										</a>
									</li>
									<li>
										<a href="#" data-scroll="information">
											<div class="inside">
												<div class="backside"> Información </div>
												<div class="frontside"> Información </div>
											</div>
										</a>
									</li>
									<li>
										<a data-toggle="modal" role="button" href="#myModal">
											<div class="inside">
												<div class="backside"> Contacto </div>
												<div class="frontside"> Contacto </div>
											</div>
										</a>
									</li>
								</ul>
							</nav>
							<div class="wr-soc">
								<div class="header-social">
									<ul class='social-transform unstyled'>
									<li>
										<a href='https://ww.facebook.com/Ciattw-Software-2197158786962674/' target='blank' class='front'><div class="fa fa-facebook"></div></a>
									</li>
									<li>
										<a href='https://www.instagram.com/ciattsoftware/' target='blank' class='front'><i class="fa fa-instagram"></i></a>
									</li>
									</ul>
								</div>
							</div>
						</div>
		    </div><!-- /.navbar-collapse -->
			</nav>
		</div>
	</div>
</header>


<!--===========================-->
<!--==========Content==========-->
<div class='main-wrapper content'>
	<section class="relative big-slider">
		<div id="form_slider" class='big-bxslider row' data-anchor="form_slider">
			<ul class="form-bxslider xSlider list-unstyled">
				<li class='firstslide'>
					<div class="container relative body-slide">
						<div class="list-forstart fin_1">
							<h2 class='h-Bold'>Sistema Online</h2>
							<p class='desc'>Sistema desarrollado para una gestión integral de cualquier tipo de gimnasio.</p>
							<ul class='ul-list-slider Open-sansR'>
								<li>Membresias y Suscripciones</li>
								<li>Control de Asistencias</li>
								<li>Encuestas</li>
								<li>Promociones</li>
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="fin_2 dv2 ver2">
							<a href="{{url('/login')}}" target="_blank" class='btn submit no-mar dark-blue-btn a-trig'>Administrador</a>
							<a href="{{url('/customer')}}" target="_blank" class='btn submit a-trig glam-btn'>App Cliente</a>
						</div>
					</div>
					<div class="img-slider slide-man1 fin_2"></div>
				</li>
				<li class='secondslide'>
					<div class="container relative body-slide">
						<div class="list-forstart fin_1">
							<h2 class='h-Bold'>App Clientes</h2>
							<p class='desc'>El sistema cuenta con una App exclusiva para que sus clientes puedran autogestionar sus pagos, asistencias y revisar su rutina de ejercicios.</p>
							<ul class='ul-list-slider Open-sansR'>
								<li>Rutina de Ejercicios</li>
								<li>Facturas y Pagos</li>
								<li>Control de Asistencias</li>
								<li>Mensajeria Interna</li>
							</ul>
						</div>
						<div class="clearfix"></div>
						<div class="fin_2 dv2 ver2">
							<a href="{{url('/login')}}" target="_blank" class='btn submit no-mar dark-blue-btn a-trig'>Administrador</a>
							<a href="{{url('/customer')}}" target="_blank" class='btn submit a-trig glam-btn'>App Cliente</a>
						</div>
					</div>
					<div class="img-slider slide-man2 fin_2"></div>
				</li>
			</ul>
		</div>
	</section>

	<section class="container" data-anchor="features">
		<div class="spacer6"></div>
			<h2 class='text-center xxh-Bold'>Principales Ventajas de <img src="{{url('landing/img/logo_grande.png')}}" style="width:200px;" alt="Improve.fit"></h2>
			<h3 class='text-center xmedium-h'>Caracteristicas del Sistema</h3>
			<div class="row trainings" id='trainings'>
				<div class="col-md-3 col-xs-6 hov1">
					<figure class='thumbnails'>
						<i class='fa fa-calendar'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Asistencia Automatica</h4>
					<div class="full-text">
						Control de Asistencia de sus clientes manual o automática con Códigos QR. 
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov2">
					<figure class='thumbnails'>
						<i class='fa fa-bullhorn'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Promociones</h4>
					<div class="full-text">
						Puede crear, enviar y gestionar promociones a sus clientes o allegados en unos pocos pasos.
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov3">
					<figure class='thumbnails'>
						<i class='fa fa-bar-chart-o'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Informes y Análisis</h4>
					<div class="full-text">
						Informes completos sobre todos los aspectos financieros y objetivos de su gimnasio.
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov4">
					<figure class='thumbnails'>
						<i class='fa fa-mobile'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Mobile App</h4>
					<div class="full-text">
						El sistema cuenta con una App descargable para sus clientes!
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov5">
					<figure class='thumbnails'>
						<i class='fa fa-flag'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Rutinas de Ejercicios</h4>
					<div class="full-text">
						Cree y asigne diferentes rutinas de ejercicios a sus clientes con descripciones e imágenes de cada uno. 
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov6">
					<figure class='thumbnails'>
						<i class='fa fa-gears'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Multisucursal</h4>
					<div class="full-text">
						Improve puede gestionar tanto una como varias sucursales al mismo tiempo junto con todos sus encargados y empleados. 
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov7">
					<figure class='thumbnails'>
						<i class='fa fa-money'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Control de Gastos</h4>
					<div class="full-text">
						Seguimiento financiero detallado con sus gastos y ganancias ordenados y gráficos.
					</div>
				</div>

				<div class="col-md-3 col-xs-6 hov8">
					<figure class='thumbnails'>
						<i class='fa fa-shield'></i>
					</figure>
					<h4 class='xxsmall-h text-center transition-h'>Roles y Permisos</h4>
					<div class="full-text">
						Regule al detalle quien puede acceder al sistema y que puede y no puede hacer asignando diferentes roles a sus usuarios.
					</div>
				</div>
			</div>
		<div class="offsetY-4"></div>
	</section>

	<section class="bg-darkblue">
		<div class="container make-row" data-anchor="information">
			<div class="row">
				<div class="col-sm-6 media-wr" id='animIt5'>
					<figure class='media-news'>
						<a href="{{url('landing/img/captura_2.jpg')}}" class="group3" title='Sistema de Gestión'>
							<img src="{{url('landing/img/captura_2.jpg')}}" alt="Captura de Pantalla 1" >
							<i class="zoom-icoBw"></i>
						</a>
					</figure>
				</div>

				<div class="col-sm-6" id='animIt6'>
					<h2 class='xh-Bold'>Sistema de Gestión</h2>
					<div class="excerpt">
						Como administrador del gimnasio cuenta con un panel de control completo de cada aspecto de tus clientes, empleados, gastos, finanzas, asistencias, facturas, rutinas y tasa de faltas y un seguimiento detallado de lo que hace cada cliente desde su App Móvil. Optimiza tus tiempos, organiza tu información, facilita la tarea administrativa con Improve.fit
						<a href="{{url('/login')}}" target="_blank" class='btn submit a-trig glam-btn'>Ir al Panel de Administración</a>
					</div>
				</div>
			</div>
			<div class="spacer2"></div>
		</div>

		<div class="container make-row">
			<div class="row">
				<div class="col-sm-6" id='animIt7'>
					<h2 class='xh-Bold'>App Móvil para Clientes</h2>
					<div class="excerpt">
						Cada cliente tiene su propio usuario y contraseña para utilizar en una App Móvil exclusiva de su gimnasio donde podrán ver sus rutinas de ejercicios, asistencia, pagos realizados y adeudados, información sobre su suscripción y un chat exclusivo con el resto de los clientes y con los administradores para evacuar posibles dudas. Toda su información en la palma de su mano. 
						<a href="{{url('/customer')}}" target="_blank" class='btn submit a-trig glam-btn'>Ir al Panel de Clientes</a>
					</div>
					<div class="spacer2"></div>
				</div>

				<div class="col-sm-6 media-wr" id='animIt8'>
					<figure class='media-news'>
						<a href="{{url('landing/img/celulares.png')}}" class="group3" title='App Móvil para Clientes'>
							<img src="{{url('landing/img/celulares.png')}}" alt="Captura de Pantalla 2" >
							<i class="zoom-icoBw"></i>
						</a>
					</figure>
				</div>
			</div>
			<div class="spacer2"></div>
		</div>

		<div class="container make-row">
			<div class="row">
				<div class="col-sm-6 media-wr" id='animIt9'>
					<figure class='media-news'>
						<a href="{{url('landing/img/captura_1.jpg')}}" class="group3" title='Toda su información organizada'>
							<img src="{{url('landing/img/captura_1.jpg')}}" alt="Captura de Pantalla 3" >
							<i class="zoom-icoBw"></i>
						</a>
					</figure>
				</div>

				<div class="col-sm-6" id='animIt10'>
					<h2 class='xh-Bold'>Toda su información organizada</h2>
					<div class="excerpt">
						Improve.fit piensa en cada aspecto que sirva para optimizar su tiempo frente a la PC, y esto incluye tener toda la información de su gimnasio visible en una pantalla principal que vera tan pronto como ingrese al sistema. Por lo que, si solamente buscaba un dato en particular, probablemente lo vea tan pronto como ingrese con su usuario y contraseña.					</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<!--===========================-->
<!--=========Footer============-->
<footer class='main-wrapper footer'>
	<div class="partners" id='partners'>
		<div class="container make-row">
			<div class="row">
				<h4 class='division-h col-md-2 dark-text'>Nuestros Clientes</h4>
				<div id='animIt16' class='col-md-2'>
					<a href="#"><img src="{{url('landing/img/staticks/vivirmejor.png')}}" alt="alt..."></a>
				</div>
				<div id='animIt17' class='col-md-2'>
					<a href="#"><img src="{{url('landing/img/staticks/cliente2.png')}}" alt="alt..."></a>
				</div>
				<div id='animIt18' class='col-md-2'>
					<a href="#"><img src="{{url('landing/img/staticks/cliente3.png')}}" alt="alt..."></a>
				</div>
				<div id='animIt19' class='col-md-2'>
					<a href="#"><img src="{{url('landing/img/staticks/cliente4.png')}}" alt="alt..."></a>
				</div>
				<div id='animIt20' class='col-md-2'>
					<a href="#"><img src="{{url('landing/img/staticks/cliente5.png')}}" alt="alt..."></a>
				</div>
			</div>
		</div>
	</div>
	<div class="container bottom">

		<ul class='social-transform footer-soc list-unstyled'>
			<li>
				<a href='https://ww.facebook.com/Ciattw-Software-2197158786962674/' target='blank' class='front'><div class="fa fa-facebook"></div></a>
			</li>
			<li>
				<a href='https://www.instagram.com/ciattsoftware/' target='blank' class='front'><i class="fa fa-instagram"></i></a>
			</li>
		</ul>
		<div class="clearifx"></div>
		<span class="copyright">
			&#169; {{date('Y')}} Improve.fit
		</span>
		<div class="container-fluid responsive-switcher hidden-md hidden-lg">
			<i class="fa fa-mobile"></i>
			Version Móvil: Habilitada
		</div>
	</div>
</footer>

<!-- Top -->
<div id="back-top-wrapper" class="visible-lg">
	<p id="back-top" class='bounceOut'>
		<a href="#top">
			<span></span>
		</a>
	</p>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" tabindex="-1" aria-hidden="true">
	<div class="modal-wr">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

		<form id='contact' action="request-form.php" method="post" accept-charset="utf-8" role="form">
			<input type="hidden" name='resultCaptcha' value=''>
			<div class='control-group'>
				<input type="text" name='name' value='' placeholder='Nombre Completo' data-required>
			</div>
			<div class='control-group'>
				<input type="text" name='email' value='' placeholder='Correo Electronico' data-required class='insert-attr'>
			</div>
			<div class='control-group'>
				<textarea name='message' cols="30" rows="10" maxlength="300" placeholder='Ingrese su mensaje ...' data-required></textarea>
			</div>
			<div class='control-group captcha'>
				<div class="picture-code">
					Cuanto es <span id="numb1">4</span> + <span id="numb2">1</span> (Anti-spam)
				</div>
				<input type="text" placeholder='Responde Aqui ...' name='check' id='chek' data-required data-pattern="5">
			</div>
			<button type="submit" value="Submit" class='btn submit' name="submit">Enviar</button>
		</form>
	</div>
</div>

</div>
	<div class="mask"></div>
	<script src="{{ asset('landing/js/libs/jquery-1.10.1.min.js') }}"></script>
	<script src="{{ asset('landing/js/libs/bootstrap.min.js') }}"></script>
	<script src="{{ asset('landing/js/cross/modernizr.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.bxslider.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.customSelect.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.colorbox-min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('landing/js/jquery.parallax-1.1.3.js') }}"></script>
	<script src="{{ asset('landing/js/custom.js') }}"></script>
	<!-- file loader -->
	<script src="{{ asset('landing/js/loader.js') }}"></script>

</body>
</html>