<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutinasTable extends Migration{

    public function up(){
        Schema::create('rutinas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }


    public function down(){
        Schema::drop('rutinas');
    }
}
