<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRutinaToClientesTable extends Migration{

    public function up(){
        Schema::table('gym_clients', function (Blueprint $table) {
            $table->integer('rutina_id')->unsigned()->nullable();
            $table->foreign('rutina_id')->references('id')->on('rutinas');
        });
    }

    public function down(){
        Schema::table('gym_clients', function (Blueprint $table) {
            $table->dropForeign(['gym_clients_rutina_id_foreign']);
            $table->dropColumn('rutina_id');
        });
    }
}
