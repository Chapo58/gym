<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriaTable extends Migration{

    public function up(){
        Schema::create('galeria', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable();
            $table->string('archivo_url');
            $table->integer('slider')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }


    public function down(){
        Schema::drop('galeria');
    }
}
