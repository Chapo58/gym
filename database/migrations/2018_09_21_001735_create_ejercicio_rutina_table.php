<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEjercicioRutinaTable extends Migration{

    public function up(){
        Schema::create('ejercicio_rutina', function(Blueprint $table) {
            $table->integer('ejercicio_id')->unsigned();
            $table->foreign('ejercicio_id')->references('id')->on('ejercicios');
            $table->integer('rutina_id')->unsigned();
            $table->foreign('rutina_id')->references('id')->on('rutinas');
            $table->string('repeticiones');
        });
    }

    public function down(){
        Schema::drop('ejercicio_rutina');
    }
}
